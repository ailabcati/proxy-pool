# ProxyPool

![build](https://github.com/Python3WebSpider/ProxyPool/workflows/build/badge.svg)
![deploy](https://github.com/Python3WebSpider/ProxyPool/workflows/deploy/badge.svg)
![](https://img.shields.io/badge/python-3.6%2B-brightgreen)
![Docker Pulls](https://img.shields.io/docker/pulls/germey/proxypool)

Simple and efficient proxy pool, providing the following functions:

- Regularly crawl free proxy websites, easy and scalable.
- Use Redis to store brokers and sort broker availability.
- Regular testing and screening to remove unavailable proxies and leave available proxies.
- Provides a proxy API to randomly select available proxies that pass the test.

The principle analysis of the proxy pool can be found in "[How to build an efficient proxy pool](https://cuiqingcai.com/7048.html)", it is recommended to read it before use.

## Ready to use

First of course is to clone the code and go into the ProxyPool folder:

````
git clone https://github.com/Python3WebSpider/ProxyPool.git
cd ProxyPool
````

Then use any of the following Docker and conventional methods to execute.

## Requirements

There are two ways to run proxy pools, one is using Docker (recommended), and the other is normal, with the following requirements:

### Docker

If you use Docker, you need to install the following environment:

- Docker
- Docker-Compose

You can search for the installation method by yourself.

Official Docker Hub image: [germey/proxypool](https://hub.docker.com/r/germey/proxypool)

### Regular way

The conventional method requires a Python environment and a Redis environment. The specific requirements are as follows:

- Python>=3.6
- Redis

## Docker run

If you have Docker and Docker-Compose installed, it only takes one command to run.

```shell script
docker-compose up
````

The result is similar to the following:

````
redis | 1:M 19 Feb 2020 17:09:43.940 * DB loaded from disk: 0.000 seconds
redis | 1:M 19 Feb 2020 17:09:43.940 * Ready to accept connections
proxypool | 2020-02-19 17:09:44,200 CRIT Supervisor is running as root. Privileges were not dropped because no user is specified in the config file. If you intend to run as root, you can set user=root in the config file to avoid this message.
proxypool | 2020-02-19 17:09:44,203 INFO supervisord started with pid 1
proxypool | 2020-02-19 17:09:45,209 INFO spawned: 'getter' with pid 10
proxypool | 2020-02-19 17:09:45,212 INFO spawned: 'server' with pid 11
proxypool | 2020-02-19 17:09:45,216 INFO spawned: 'tester' with pid 12
proxypool | 2020-02-19 17:09:46,596 INFO success: getter entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
proxypool | 2020-02-19 17:09:46,596 INFO success: server entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
proxypool | 2020-02-19 17:09:46,596 INFO success: tester entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
````

You can see that Redis, Getter, Server, and Tester have all started successfully.

At this time, visit [http://localhost:5555/random](http://localhost:5555/random) to get a random available proxy.

Of course, you can also choose to build by yourself and run the following commands directly:

````
docker-compose -f build.yaml up
````

If the download speed is particularly slow, you can modify the Dockerfile yourself, modify:

````diff
- RUN pip install -r requirements.txt
+ RUN pip install -r requirements.txt -i https://pypi.douban.com/simple
````

## run as usual

If you do not use Docker to run, you can also run it after configuring the Python and Redis environments. The steps are as follows.

### Install and configure Redis

It is possible to install Redis locally, start Redis with Docker, and remotely Redis, as long as it can be connected normally.

First of all, you can need some environment variables, and the proxy pool will read these values ​​through environment variables.

There are two ways to set the environment variables of Redis. One is to set the host, port and password respectively, and the other is to set the connection string. The setting methods are as follows:

Set host, port, and password. If password is empty, it can be set to an empty string. The example is as follows:

```shell script
export PROXYPOOL_REDIS_HOST='localhost'
export PROXYPOOL_REDIS_PORT=6379
export PROXYPOOL_REDIS_PASSWORD=''
export PROXYPOOL_REDIS_DB=0
````

Or just set the connection string:

```shell script
export PROXYPOOL_REDIS_CONNECTION_STRING='redis://localhost'
````

The format of the connection string here needs to conform to the format of `redis://[:password@]host[:port][/database]`,
The square bracket parameter can be omitted, the default value of port is 6379, the default value of database is 0, and the default value of password is empty.

You can choose one of the above two settings.

### Install dependencies

[Conda] is highly recommended here (https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
Or [virtualenv](https://virtualenv.pypa.io/en/latest/user_guide.html) to create a virtual environment with a Python version no lower than 3.6.

Then pip install the dependencies:

```shell script
pip3 install -r requirements.txt
````

### Running the proxy pool

There are two ways to run the proxy pool, one is to run all Tester, Getter, and Server, and the other is to run separately on demand.

Generally speaking, you can choose to run all, the command is as follows:

```shell script
python3 run.py
````

After running, Tester, Getter, and Server will be started. At this time, visit [http://localhost:5555/random](http://localhost:5555/random) to get a random available proxy.

Or if you figure out the architecture of the proxy pool, you can run it separately on demand with the following commands:

```shell script
python3 run.py --processor getter
python3 run.py --processor tester
python3 run.py --processor server
````

Here processor can specify whether to run Tester, Getter or Server.

## use

After running successfully, you can get a random available proxy through [http://localhost:5555/random](http://localhost:5555/random).

It can be implemented by program docking. The following example shows the process of obtaining the proxy and crawling the web page:

```python
import requests

proxypool_url = 'http://127.0.0.1:5555/random'
target_url = 'http://httpbin.org/get'

def get_random_proxy():
    """
    get random proxy from proxypool
    :return: proxy
    """
    return requests.get(proxypool_url).text.strip()

def crawl(url, proxy):
    """
    use proxy to crawl page
    :param url: page url
    :param proxy: proxy, such as 8.8.8.8:8888
    :return: html
    """
    proxies = {'http': 'http://' + proxy}
    return requests.get(url, proxies=proxies).text


def main():
    """
    main method, entry point
    :return: none
    """
    proxy = get_random_proxy()
    print('get random proxy', proxy)
    html = crawl(target_url, proxy)
    print(html)

if __name__ == '__main__':
    main()

The results are as follows:

````
get random proxy 116.196.115.209:8080
{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "httpbin.org",
    "User-Agent": "python-requests/2.22.0",
    "X-Amzn-Trace-Id": "Root=1-5e4d7140-662d9053c0a2e513c7278364"
  },
  "origin": "116.196.115.209",
  "url": "https://httpbin.org/get"
}
````

You can see that the proxy was successfully obtained, and httpbin.org was requested to verify the availability of the proxy.

## Configurable items

The proxy pool can be configured with some parameters by setting environment variables.

### switch

- ENABLE_TESTER: Allow Tester to start, default true
- ENABLE_GETTER: Allow Getter to start, default true
- ENABLE_SERVER: run Server startup, default true


### environment

- APP_ENV: Running environment, you can set dev, test, prod, namely development, test, production environment, default dev
- APP_DEBUG: debug mode, can be set to true or false, the default is true
- APP_PROD_METHOD: The official environment to start the application method, the default is `gevent`,
  Optional: `tornado`, `meinheld` (requires tornado or meinheld module installed respectively)

### Redis connection

- PROXYPOOL_REDIS_HOST / REDIS_HOST: Host of Redis, where PROXYPOOL_REDIS_HOST will override the value of REDIS_HOST.
- PROXYPOOL_REDIS_PORT / REDIS_PORT: the port of Redis, where PROXYPOOL_REDIS_PORT will override the value of REDIS_PORT.
- PROXYPOOL_REDIS_PASSWORD / REDIS_PASSWORD: Redis password, where PROXYPOOL_REDIS_PASSWORD will override the value of REDIS_PASSWORD.
- PROXYPOOL_REDIS_DB / REDIS_DB: Redis database index, such as 0, 1, where PROXYPOOL_REDIS_DB will overwrite the value of REDIS_DB.
- PROXYPOOL_REDIS_CONNECTION_STRING / REDIS_CONNECTION_STRING: Redis connection string, where PROXYPOOL_REDIS_CONNECTION_STRING overrides the value of REDIS_CONNECTION_STRING.
- PROXYPOOL_REDIS_KEY / REDIS_KEY: The name of the dictionary used by the Redis storage proxy, where PROXYPOOL_REDIS_KEY will override the value of REDIS_KEY.

### Processor

- CYCLE_TESTER: Tester running cycle, that is, how often to run tests at intervals, the default is 20 seconds
- CYCLE_GETTER: Getter running cycle, that is, how often to run the agent to get it, the default is 100 seconds
- TEST_URL: Test URL, default Baidu
- TEST_TIMEOUT: Test timeout, default 10 seconds
- TEST_BATCH: The number of batch tests, the default is 20 agents
- TEST_VALID_STATUS: Is the test valid status
- API_HOST: Proxy Server runs Host, default 0.0.0.0
- API_PORT: Proxy Server running port, default 5555
- API_THREADED: Whether the proxy server uses multi-threading, the default is true

### log

- LOG_DIR: log relative path
- LOG_RUNTIME_FILE: Run log file name
- LOG_ERROR_FILE: Error log file name
- ENABLE_LOG_FILE: Whether to output the log file, the default is true, if set to false, then neither ENABLE_LOG_RUNTIME_FILE nor ENABLE_LOG_ERROR_FILE will take effect
- ENABLE_LOG_RUNTIME_FILE: Whether to output the runtime log file, the default is true
- ENABLE_LOG_ERROR_FILE: whether to output the error log file, the default is true

The above content can be configured using environment variables, that is, you can set the corresponding environment variable values ​​before running, such as changing the test address and Redis key name:

```shell script
export TEST_URL=http://weibo.cn
export REDIS_KEY=proxies:weibo
````

A proxy pool dedicated to Weibo can be built, and all effective proxies can crawl Weibo.

If you use Docker-Compose to start the proxy pool, you need to specify environment variables in the docker-compose.yml file, such as:

````yaml
version: "3"
services:
  redis:
    image: redis:alpine
    container_name: redis
    command: redis-server
    ports:
      - "6379:6379"
    restart: always
  proxypool:
    build: .
    image: "germey/proxypool"
    container_name: proxypool
    ports:
      - "5555:5555"
    restart: always
    environment:
      REDIS_HOST: redis
      TEST_URL: http://weibo.cn
      REDIS_KEY: proxies:weibo
````

## Extending the proxy crawler

The crawlers of the proxy are placed in the proxypool/crawlers folder, and currently a limited number of proxy crawlers are connected.

To expand a crawler, just create a new Python file in the crawlers folder and declare a Class.

The writing specification is as follows:

````python
from pyquery import PyQuery as pq
from proxypool.schemas.proxy import Proxy
from proxypool.crawlers.base import BaseCrawler

BASE_URL = 'http://www.664ip.cn/{page}.html'
MAX_PAGE = 5

class Daili66Crawler(BaseCrawler):
    """
    daili66 crawler, http://www.66ip.cn/1.html
    """
    urls = [BASE_URL.format(page=page) for page in range(1, MAX_PAGE + 1)]

    def parse(self, html):
        """
        parse html file to get proxies
        :return:
        """
        doc = pq(html)
        trs = doc('.containerbox table tr:gt(0)').items()
        for tr in trs:
            host = tr.find('td:nth-child(1)').text()
            port = int(tr.find('td:nth-child(2)').text())
            yield Proxy(host=host, port=port)
````

Here you only need to define a Crawler that inherits BaseCrawler, and then define the urls variable and the parse method.

- The urls variable is the list of proxy website URLs to be crawled, which can be defined by programs or written as fixed content.
- The parse method receives one parameter, html, the html of the proxy URL. In the parse method, you only need to write the html parsing, parse out the host and port, and construct a Proxy object yield return.

The crawling of web pages does not need to be implemented. BaseCrawler already has a default implementation. If you need to change the crawling method, you can rewrite the crawl method.

You are welcome to send more Pull Requests to contribute to Crawler to make its proxy source richer and more powerful.

## deploy

This project provides Kubernetes deployment scripts. To deploy to Kubernetes, please refer to [kubernetes](./kubernetes).

## To be developed

- [ ] Front-end page management
- [ ] Statistical analysis of usage

If you are interested in developing together, you can leave a message in the Issue, thank you very much!

##LICENSE

MIT
